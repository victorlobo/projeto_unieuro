<?php
namespace controller\aluno;
require_once(__DIR__ . '/../../autoload.php');

use controller\usuario\UsuarioController;
use config\Connection;

class AlunoController
{
    public static function buscaGradeHoraria(){
        $Aluno = UsuarioController::usuarioLogado();
        $sql = "
            SELECT C.curso, D.sigla, D.disciplina, PE.nome AS professor, DS.diaSemana, H.horario
            FROM aluno AS A
            INNER JOIN aluno_turma AS ATu ON A.idAluno = ATu.idAluno
            INNER JOIN turma AS T ON ATu.idTurma = T.idTurma
            INNER JOIN turma_curso_disciplina AS TCD ON T.idTurma = TCD.idTurma
            INNER JOIN curso AS C ON TCD.idCurso = C.idCurso
            INNER JOIN disciplina AS D ON TCD.idDisciplina = D.idDisciplina
            INNER JOIN professor AS PR ON TCD.idProfessor = PR.idProfessor
            INNER JOIN pessoa AS PE ON PR.idPessoa = PE.idPessoa
            INNER JOIN dia_semana AS DS ON TCD.idDiaSemana = DS.idDiaSemana
            INNER JOIN horario AS H ON TCD.idHorario = H.idHorario
            WHERE A.idAluno = " . $Aluno->getIdAluno() . "
            ORDER BY H.idHorario ASC,
            DS.idDiaSemana ASC";

        return Connection::fetchSql($sql, 'all');
    }
}