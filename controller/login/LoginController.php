<?php

namespace controller\login;

require(__DIR__ . '/../../autoload.php');

use model\AlunoModel;
use model\ProfessorModel;
use config\Connection;

class LoginController
{
    public function login(){
        $login = $_POST['login'];
        $senha = md5($_POST['senha']);
        $tipoUsuario = $_POST['tipoUsuario'];

        if($tipoUsuario == 'aluno'){
            $Usuario = new AlunoModel();
        }
        else if($tipoUsuario == 'pessoa'){
            $Usuario = new ProfessorModel();
        }
        else{
            return false;
        }

        $Usuario->findByLoginSenha($Usuario, $login, $senha);
        if(!empty($Usuario)){
            $this->realizaLogin($Usuario);
        }
    }

    private function realizaLogin($user){
        $_SESSION['usuario_logado'] = serialize($user);
        header('Location: /aluno/inicio');
    }

    public static function realizaLogout(){
        unset($_SESSION['usuario_logado']);
        header('Location: /login/login.php');
    }
}