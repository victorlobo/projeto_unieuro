<?php
namespace controller\usuario;

class UsuarioController
{
    public static function usuarioLogado(){
        $Usuario = false;
        if(isset($_SESSION['usuario_logado'])) {
            $Usuario = unserialize($_SESSION['usuario_logado']);
        }
        return $Usuario;
    }
}