<?php

namespace model;

use config\Connection;

class PessoaModel
{
    private $idPessoa;
    private $nome;
    private $dataNascimento;
    private $endereco;
    private $telefone;
    private $email;

    public static function tableName(){
        return 'pessoa';
    }
    
    public function getIdPessoa(){
        return $this->idPessoa;
    }
    
    public function setIdPessoa($idPessoa){
        $this->idPessoa = $idPessoa;
    }

    public function getNome(){
        return $this->nome;
    }

    public function setNome($nome){
        $this->nome = $nome;
    }

    public function getDataNascimento(){
        return $this->dataNascimento;
    }

    public function setDataNascimento($dataNascimento){
        $this->dataNascimento = $dataNascimento;
    }

    public function getEndereco(){
        return $this->endereco;
    }

    public function setEndereco($endereco){
        $this->endereco = $endereco;
    }

    public function getTelefone(){
        return $this->telefone;
    }

    public function setTelefone($telefone){
        $this->telefone = $telefone;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function load($pessoa){
        $this->idPessoa = $pessoa->idPessoa;
        $this->nome = $pessoa->nome;
        $this->dataNascimento = $pessoa->dataNascimento;
        $this->endereco = $pessoa->endereco;
        $this->telefone = $pessoa->telefone;
        $this->email = $pessoa->email;
    }

    public function findByLoginSenha($Usuario, $login, $senha){
        $sql = "
        SELECT *
        FROM pessoa AS P
        INNER JOIN " . $Usuario::tableName() . " AS U
        ON P.idPessoa = U.idPessoa
        WHERE U.login = '$login'
        AND U.senha = '$senha'";

        $rs = Connection::fetchSql($sql, 'row');
        if(!empty($rs)){
            $Usuario->load($rs);
        }
    }
}