<?php

namespace model;


class ProfessorModel extends PessoaModel
{
    private $idProfessor;
    private $idPessoa;
    private $senha;
    private $login;
    private $nivelEnsino;
    private $formacao;

    public static function tableName(){
        return 'professor';
    }

    public function getIdProfessor()
    {
        return $this->idProfessor;
    }

    public function setIdProfessor($idProfessor)
    {
        $this->idProfessor = $idProfessor;
    }

    public function getIdPessoa()
    {
        return $this->idPessoa;
    }

    public function setIdPessoa($idPessoa)
    {
        $this->idPessoa = $idPessoa;
    }

    public function getSenha()
    {
        return $this->senha;
    }

    public function setSenha($senha)
    {
        $this->senha = $senha;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin($login)
    {
        $this->login = $login;
    }

    public function getNivelEnsino()
    {
        return $this->nivelEnsino;
    }

    public function setNivelEnsino($nivelEnsino)
    {
        $this->nivelEnsino = $nivelEnsino;
    }

    public function getFormacao()
    {
        return $this->formacao;
    }

    public function setFormacao($formacao)
    {
        $this->formacao = $formacao;
    }
}