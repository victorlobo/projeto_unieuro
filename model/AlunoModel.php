<?php
namespace model;

require(__DIR__ . '/../autoload.php');

class AlunoModel extends PessoaModel
{
    private $idAluno;
    private $login;
    private $senha;

    public static function tableName(){
        return 'aluno';
    }

    public function getIdAluno(){
        return $this->idAluno;
    }

    public function setIdAluno($idAluno){
        $this->idAluno = $idAluno;
    }

    public function getLogin(){
        return $this->login;
    }

    public function setLogin($login){
        $this->login = $login;
    }

    public function getSenha(){
        return $this->senha;
    }

    public function setSenha($senha){
        $this->senha = $senha;
    }

    public function load($usuario){
        parent::load($usuario);
        $this->idAluno = $usuario->idAluno;
        $this->login = $usuario->login;
        $this->senha = $usuario->senha;
    }
}