-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema unieuro
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `unieuro` DEFAULT CHARACTER SET utf8 ;
USE `unieuro` ;

-- -----------------------------------------------------
-- Table `unieuro`.`pessoa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `unieuro`.`pessoa` (
  `idPessoa` INT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  `dataNascimento` DATE NOT NULL,
  `endereco` VARCHAR(60) NULL,
  `telefone` VARCHAR(45) NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idPessoa`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `unieuro`.`professor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `unieuro`.`professor` (
  `idProfessor` INT NOT NULL AUTO_INCREMENT,
  `idPessoa` INT NOT NULL,
  `senha` VARCHAR(45) NOT NULL,
  `login` VARCHAR(45) NOT NULL,
  `nivelEnsino` VARCHAR(45) NULL,
  `formacao` VARCHAR(45) NULL,
  PRIMARY KEY (`idProfessor`),
  CONSTRAINT `fk_professor_pessoa`
    FOREIGN KEY (`idPessoa`)
    REFERENCES `unieuro`.`pessoa` (`idPessoa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_professor_pessoa_idx` ON `unieuro`.`professor` (`idPessoa` ASC);


-- -----------------------------------------------------
-- Table `unieuro`.`aluno`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `unieuro`.`aluno` (
  `idAluno` INT NOT NULL AUTO_INCREMENT,
  `idPessoa` INT NOT NULL,
  `login` VARCHAR(45) NOT NULL,
  `senha` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idAluno`),
  CONSTRAINT `fk_aluno_pessoa1`
    FOREIGN KEY (`idPessoa`)
    REFERENCES `unieuro`.`pessoa` (`idPessoa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_aluno_pessoa1_idx` ON `unieuro`.`aluno` (`idPessoa` ASC);


-- -----------------------------------------------------
-- Table `unieuro`.`disciplina`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `unieuro`.`disciplina` (
  `idDisciplina` INT NOT NULL,
  `disciplina` VARCHAR(45) NOT NULL,
  `sigla` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`idDisciplina`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `unieuro`.`turma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `unieuro`.`turma` (
  `idTurma` INT NOT NULL,
  `semestreAtual` INT NOT NULL,
  PRIMARY KEY (`idTurma`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `unieuro`.`curso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `unieuro`.`curso` (
  `idCurso` INT NOT NULL AUTO_INCREMENT,
  `curso` VARCHAR(45) NOT NULL,
  `descricao` LONGTEXT NULL,
  `duracao` INT NOT NULL COMMENT 'Valor em semestres',
  PRIMARY KEY (`idCurso`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `unieuro`.`aluno_turma`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `unieuro`.`aluno_turma` (
  `idAluno` INT NOT NULL,
  `idTurma` INT NOT NULL,
  PRIMARY KEY (`idAluno`, `idTurma`),
  CONSTRAINT `fk_aluno_has_turma_aluno1`
    FOREIGN KEY (`idAluno`)
    REFERENCES `unieuro`.`aluno` (`idAluno`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_aluno_has_turma_turma1`
    FOREIGN KEY (`idTurma`)
    REFERENCES `unieuro`.`turma` (`idTurma`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_aluno_has_turma_turma1_idx` ON `unieuro`.`aluno_turma` (`idTurma` ASC);

CREATE INDEX `fk_aluno_has_turma_aluno1_idx` ON `unieuro`.`aluno_turma` (`idAluno` ASC);


-- -----------------------------------------------------
-- Table `unieuro`.`horario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `unieuro`.`horario` (
  `idHorario` INT NOT NULL AUTO_INCREMENT,
  `horario` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`idHorario`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `unieuro`.`dia_semana`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `unieuro`.`dia_semana` (
  `idDiaSemana` INT NOT NULL AUTO_INCREMENT,
  `diaSemana` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idDiaSemana`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `unieuro`.`turma_curso_disciplina`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `unieuro`.`turma_curso_disciplina` (
  `idTurma` INT NOT NULL,
  `idSemestre` INT NOT NULL,
  `idProfessor` INT NOT NULL,
  `idCurso` INT NOT NULL,
  `idDisciplina` INT NOT NULL,
  `idDiaSemana` INT NOT NULL,
  `idHorario` INT NOT NULL,
  PRIMARY KEY (`idTurma`, `idSemestre`, `idDisciplina`, `idHorario`),
  CONSTRAINT `fk_turma_has_curso_disciplina_turma1`
    FOREIGN KEY (`idTurma`)
    REFERENCES `unieuro`.`turma` (`idTurma`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_turma_has_curso_disciplina_professor1`
    FOREIGN KEY (`idProfessor`)
    REFERENCES `unieuro`.`professor` (`idProfessor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_turma_curso_disciplina_disciplina1`
    FOREIGN KEY (`idDisciplina`)
    REFERENCES `unieuro`.`disciplina` (`idDisciplina`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_turma_curso_disciplina_curso1`
    FOREIGN KEY (`idCurso`)
    REFERENCES `unieuro`.`curso` (`idCurso`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_turma_curso_disciplina_dia_semana1`
    FOREIGN KEY (`idDiaSemana`)
    REFERENCES `unieuro`.`dia_semana` (`idDiaSemana`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_turma_curso_disciplina_horario1`
    FOREIGN KEY (`idHorario`)
    REFERENCES `unieuro`.`horario` (`idHorario`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_turma_has_curso_disciplina_turma1_idx` ON `unieuro`.`turma_curso_disciplina` (`idTurma` ASC);

CREATE INDEX `fk_turma_has_curso_disciplina_professor1_idx` ON `unieuro`.`turma_curso_disciplina` (`idProfessor` ASC);

CREATE INDEX `fk_turma_curso_disciplina_disciplina1_idx` ON `unieuro`.`turma_curso_disciplina` (`idDisciplina` ASC);

CREATE INDEX `fk_turma_curso_disciplina_curso1_idx` ON `unieuro`.`turma_curso_disciplina` (`idCurso` ASC);

CREATE INDEX `fk_turma_curso_disciplina_dia_semana1_idx` ON `unieuro`.`turma_curso_disciplina` (`idDiaSemana` ASC);

CREATE INDEX `fk_turma_curso_disciplina_horario1_idx` ON `unieuro`.`turma_curso_disciplina` (`idHorario` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `unieuro`.`pessoa`
-- -----------------------------------------------------
START TRANSACTION;
USE `unieuro`;
INSERT INTO `unieuro`.`pessoa` (`idPessoa`, `nome`, `dataNascimento`, `endereco`, `telefone`, `email`) VALUES (1, 'Victor Lobo', '1992-05-28', 'QND19 Lote 01', '61987654321', 'victorlobo@gmail.com');
INSERT INTO `unieuro`.`pessoa` (`idPessoa`, `nome`, `dataNascimento`, `endereco`, `telefone`, `email`) VALUES (2, 'Iasmini', '1995-08-13', 'Rua das Palmeiras Lote 07 - Águas Claras', '61981234567', 'aldo@gmail.com');
INSERT INTO `unieuro`.`pessoa` (`idPessoa`, `nome`, `dataNascimento`, `endereco`, `telefone`, `email`) VALUES (3, 'Cleber', '1995-08-13', 'Rua das Palmeiras Lote 07 - Águas Claras', '61981234567', 'aldo@gmail.com');
INSERT INTO `unieuro`.`pessoa` (`idPessoa`, `nome`, `dataNascimento`, `endereco`, `telefone`, `email`) VALUES (4, 'Santana', '1995-08-13', 'Rua das Palmeiras Lote 07 - Águas Claras', '61981234567', 'aldo@gmail.com');
INSERT INTO `unieuro`.`pessoa` (`idPessoa`, `nome`, `dataNascimento`, `endereco`, `telefone`, `email`) VALUES (5, 'Michel', '1995-08-13', 'Rua das Palmeiras Lote 07 - Águas Claras', '61981234567', 'aldo@gmail.com');
INSERT INTO `unieuro`.`pessoa` (`idPessoa`, `nome`, `dataNascimento`, `endereco`, `telefone`, `email`) VALUES (6, 'Aldo', '1995-08-13', 'Rua das Palmeiras Lote 07 - Águas Claras', '61981234567', 'aldo@gmail.com');

COMMIT;


-- -----------------------------------------------------
-- Data for table `unieuro`.`professor`
-- -----------------------------------------------------
START TRANSACTION;
USE `unieuro`;
INSERT INTO `unieuro`.`professor` (`idProfessor`, `idPessoa`, `senha`, `login`, `nivelEnsino`, `formacao`) VALUES (1, 2, '202cb962ac59075b964b07152d234b70', 'iasmini', 'Mestrado', 'Ciência da Computação');
INSERT INTO `unieuro`.`professor` (`idProfessor`, `idPessoa`, `senha`, `login`, `nivelEnsino`, `formacao`) VALUES (2, 3, '202cb962ac59075b964b07152d234b70', 'cleber', 'Mestrado', 'Ciência da Computação');
INSERT INTO `unieuro`.`professor` (`idProfessor`, `idPessoa`, `senha`, `login`, `nivelEnsino`, `formacao`) VALUES (3, 4, '202cb962ac59075b964b07152d234b70', 'santana', 'Mestrado', 'Ciência da Computação');
INSERT INTO `unieuro`.`professor` (`idProfessor`, `idPessoa`, `senha`, `login`, `nivelEnsino`, `formacao`) VALUES (4, 5, '202cb962ac59075b964b07152d234b70', 'michel', 'Mestrado', 'Ciência da Computação');
INSERT INTO `unieuro`.`professor` (`idProfessor`, `idPessoa`, `senha`, `login`, `nivelEnsino`, `formacao`) VALUES (5, 6, '202cb962ac59075b964b07152d234b70', 'aldo', 'Mestrado', 'Ciência da Computação');

COMMIT;


-- -----------------------------------------------------
-- Data for table `unieuro`.`aluno`
-- -----------------------------------------------------
START TRANSACTION;
USE `unieuro`;
INSERT INTO `unieuro`.`aluno` (`idAluno`, `idPessoa`, `login`, `senha`) VALUES (1, 1, 'victor', '202cb962ac59075b964b07152d234b70');

COMMIT;


-- -----------------------------------------------------
-- Data for table `unieuro`.`disciplina`
-- -----------------------------------------------------
START TRANSACTION;
USE `unieuro`;
INSERT INTO `unieuro`.`disciplina` (`idDisciplina`, `disciplina`, `sigla`) VALUES (1, 'Computação Forense', 'FORCP');
INSERT INTO `unieuro`.`disciplina` (`idDisciplina`, `disciplina`, `sigla`) VALUES (2, 'Segurança da Informação', 'SEG');
INSERT INTO `unieuro`.`disciplina` (`idDisciplina`, `disciplina`, `sigla`) VALUES (3, 'Auditoria em TI', 'AUDTI');
INSERT INTO `unieuro`.`disciplina` (`idDisciplina`, `disciplina`, `sigla`) VALUES (4, 'Gestão de Risco', 'GRIS');
INSERT INTO `unieuro`.`disciplina` (`idDisciplina`, `disciplina`, `sigla`) VALUES (5, 'Projeto Integrador', 'PISEGINF');

COMMIT;


-- -----------------------------------------------------
-- Data for table `unieuro`.`turma`
-- -----------------------------------------------------
START TRANSACTION;
USE `unieuro`;
INSERT INTO `unieuro`.`turma` (`idTurma`, `semestreAtual`) VALUES (1, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `unieuro`.`curso`
-- -----------------------------------------------------
START TRANSACTION;
USE `unieuro`;
INSERT INTO `unieuro`.`curso` (`idCurso`, `curso`, `descricao`, `duracao`) VALUES (1, 'Sistemas de Informação', 'Curso para profissionais que querem trabalhar na gestão e desenvolvimento de projetos de sistemas de informação', 8);

COMMIT;


-- -----------------------------------------------------
-- Data for table `unieuro`.`aluno_turma`
-- -----------------------------------------------------
START TRANSACTION;
USE `unieuro`;
INSERT INTO `unieuro`.`aluno_turma` (`idAluno`, `idTurma`) VALUES (1, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `unieuro`.`horario`
-- -----------------------------------------------------
START TRANSACTION;
USE `unieuro`;
INSERT INTO `unieuro`.`horario` (`idHorario`, `horario`) VALUES (1, '19:00 às 20:40');
INSERT INTO `unieuro`.`horario` (`idHorario`, `horario`) VALUES (2, '20:50 às 21:40');

COMMIT;


-- -----------------------------------------------------
-- Data for table `unieuro`.`dia_semana`
-- -----------------------------------------------------
START TRANSACTION;
USE `unieuro`;
INSERT INTO `unieuro`.`dia_semana` (`idDiaSemana`, `diaSemana`) VALUES (1, 'Segunda');
INSERT INTO `unieuro`.`dia_semana` (`idDiaSemana`, `diaSemana`) VALUES (2, 'Terça');
INSERT INTO `unieuro`.`dia_semana` (`idDiaSemana`, `diaSemana`) VALUES (3, 'Quarta');
INSERT INTO `unieuro`.`dia_semana` (`idDiaSemana`, `diaSemana`) VALUES (4, 'Quita');
INSERT INTO `unieuro`.`dia_semana` (`idDiaSemana`, `diaSemana`) VALUES (5, 'Sexta');

COMMIT;


-- -----------------------------------------------------
-- Data for table `unieuro`.`turma_curso_disciplina`
-- -----------------------------------------------------
START TRANSACTION;
USE `unieuro`;
INSERT INTO `unieuro`.`turma_curso_disciplina` (`idTurma`, `idSemestre`, `idProfessor`, `idCurso`, `idDisciplina`, `idDiaSemana`, `idHorario`) VALUES (1, 1, 1, 1, 1, 1, 1);
INSERT INTO `unieuro`.`turma_curso_disciplina` (`idTurma`, `idSemestre`, `idProfessor`, `idCurso`, `idDisciplina`, `idDiaSemana`, `idHorario`) VALUES (1, 1, 1, 1, 1, 1, 2);
INSERT INTO `unieuro`.`turma_curso_disciplina` (`idTurma`, `idSemestre`, `idProfessor`, `idCurso`, `idDisciplina`, `idDiaSemana`, `idHorario`) VALUES (1, 1, 2, 1, 2, 2, 1);
INSERT INTO `unieuro`.`turma_curso_disciplina` (`idTurma`, `idSemestre`, `idProfessor`, `idCurso`, `idDisciplina`, `idDiaSemana`, `idHorario`) VALUES (1, 1, 2, 1, 2, 2, 2);
INSERT INTO `unieuro`.`turma_curso_disciplina` (`idTurma`, `idSemestre`, `idProfessor`, `idCurso`, `idDisciplina`, `idDiaSemana`, `idHorario`) VALUES (1, 1, 3, 1, 3, 3, 1);
INSERT INTO `unieuro`.`turma_curso_disciplina` (`idTurma`, `idSemestre`, `idProfessor`, `idCurso`, `idDisciplina`, `idDiaSemana`, `idHorario`) VALUES (1, 1, 3, 1, 3, 3, 2);
INSERT INTO `unieuro`.`turma_curso_disciplina` (`idTurma`, `idSemestre`, `idProfessor`, `idCurso`, `idDisciplina`, `idDiaSemana`, `idHorario`) VALUES (1, 1, 2, 1, 4, 4, 1);
INSERT INTO `unieuro`.`turma_curso_disciplina` (`idTurma`, `idSemestre`, `idProfessor`, `idCurso`, `idDisciplina`, `idDiaSemana`, `idHorario`) VALUES (1, 1, 2, 1, 4, 4, 2);
INSERT INTO `unieuro`.`turma_curso_disciplina` (`idTurma`, `idSemestre`, `idProfessor`, `idCurso`, `idDisciplina`, `idDiaSemana`, `idHorario`) VALUES (1, 1, 4, 1, 5, 5, 1);
INSERT INTO `unieuro`.`turma_curso_disciplina` (`idTurma`, `idSemestre`, `idProfessor`, `idCurso`, `idDisciplina`, `idDiaSemana`, `idHorario`) VALUES (1, 1, 4, 1, 5, 5, 2);

COMMIT;

