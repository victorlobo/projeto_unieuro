        <footer>
            <nav class="navbar navbar-default navbar-fixed-bottom">
                <div class="container">
                    <p class="navbar-text">Desenvolvido por <strong>Victor Lobo</strong></p>
                    <p class="navbar-text pull-right">Todos os direitos reservados &copy; <?=date('Y')?></p>
                </div>
            </nav>
        </footer>
        <script src="/assets/jquery/jquery-3.3.1.min.js"></script>
        <script src="/assets/bootstrap/js/bootstrap.min.js"></script>
        [CUSTOM_SCRIPTS]
    </body>
</html>