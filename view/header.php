<?php
require_once(__DIR__ . '/../autoload.php');
use controller\usuario\UsuarioController;

function getHeader($active)
{
    $UsuarioController = new UsuarioController();
    $Usuario = $UsuarioController->usuarioLogado();
    $linkInicio = "#";
    if($Usuario){
        if(get_class($Usuario) == "model\AlunoModel"){
            $linkInicio = "/aluno/inicio";
        }
        else if(get_class($Usuario) == "model\ProfessorModel"){
            $linkInicio = "/professor/inicio";
        }
    }

    $inicio = ['class'=>'', 'small-devices' => ''];
    $menu_2 = ['class'=>'', 'small-devices' => ''];
//    $menu_3 = ['class'=>'dropdown', 'small-devices' => ''];
    $meus_dados = ['class'=>'dropdown', 'small-devices' => ''];
    ${$active}['class'] .= ' active';
    ${$active}['small-devices'] = '<span class="sr-only">(current)</span>';
    $inicio['class'] = 'class="' . $inicio['class'] . '"';
    $menu_2['class'] = 'class="' . $menu_2['class'] . '"';
//    $menu_3['class'] = 'class="' . $menu_3['class'] . '"';
    $meus_dados['class'] = 'class="' . $meus_dados['class'] . '"';
    ?>

    <header>
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Unieuro</a>
                </div>
                <?php
                if($Usuario) {
                    ?>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li id="menu_1" <?= $inicio['class'] ?>><a
                                    href="<?= $linkInicio; ?>">Início <?= $inicio['small-devices'] ?></a></li>
                            <li id="menu_2" <?= $menu_2['class'] ?>><a href="#">Meus
                                    Cursos <?= $menu_2['small-devices'] ?></a></li>
                            <?php /*
                        <li id="menu_3" <?=$menu_3['class']?>>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span> <?=$aluno['small-devices']?></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">One more separated link</a></li>
                            </ul>
                        </li>
                        */ ?>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li id="meus_dados" <?= $meus_dados['class'] ?>>
                                <a href="/aluno/meus-dados">Meus Dados <?= $meus_dados['small-devices'] ?></a>
                            </li>
                            <li><a href="/login/logout.php">Sair</a></li>
                        </ul>
                        <form class="navbar-form navbar-right">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Pesquisa">
                            </div>
                            <button type="submit" class="btn btn-default">Buscar</button>
                        </form>
                    </div><!-- /.navbar-collapse -->
                    <?php
                }
                ?>
            </div><!-- /.container-fluid -->
        </nav>
    </header>
    <?php
}
?>