<?php
require_once(__DIR__ . '/../../autoload.php');
use controller\usuario\UsuarioController;
use view\PaginaHTML;

$UsuarioController = new UsuarioController();
$Usuario = $UsuarioController->usuarioLogado();
if(!$Usuario){
    header('Location: /login/login.php');
}
$PaginaHTML = new PaginaHTML();
$PaginaHTML->titulo = "Meus Dados";
$idade = date('Y-m-d') - $Usuario->getDataNascimento();
?>
<?=$PaginaHTML->getHead()?>
<?php $PaginaHTML->getHeader('meus_dados') ?>
    <section id="main-content">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="text-center"><?=$PaginaHTML->titulo?></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <p><strong>Nome: </strong> <?=$Usuario->getNome()?></p>
                </div>
                <div class="col-md-4">
                    <p><strong>E-mail: </strong> <?=$Usuario->getEmail()?></p>
                </div>
                <div class="col-md-4">
                    <p><strong>Login: </strong> <?=$Usuario->getLogin()?></p>
                </div>
                <div class="col-md-4">
                    <p><strong>Idade: </strong> <?=$idade?></p>
                </div>
                <div class="col-md-4">
                    <p><strong>Telefone: </strong> <?=$Usuario->getTelefone()?></p>
                </div>
                <div class="col-md-4">
                    <p><strong>Endereço: </strong> <?=$Usuario->getEndereco()?></p>
                </div>
                <div class="col-md-12">
                    <p><a href="#">Alterar Senha</a></p>
                </div>
            </div>
        </div>
    </section>
<?=$PaginaHTML->getFooter()?>