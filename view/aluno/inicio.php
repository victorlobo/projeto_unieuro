<?php
require_once(__DIR__ . '/../../autoload.php');
use controller\aluno\AlunoController;
use controller\usuario\UsuarioController;
use view\PaginaHTML;

$UsuarioController = new UsuarioController();
$Usuario = $UsuarioController->usuarioLogado();
if(!$Usuario){
    header('Location: /login/login.php');
}
$PaginaHTML = new PaginaHTML();
$PaginaHTML->titulo = "Início";
$gradeHorariaDisciplina = (array)AlunoController::buscaGradeHoraria();
$curso = $gradeHorariaDisciplina[0]['curso'];
$gradeHoraria = array();
foreach($gradeHorariaDisciplina AS $grade){
    $gradeHoraria[$grade['horario']][$grade['diaSemana']] = [
        'sigla' => $grade['sigla'],
        'disciplina' => $grade['disciplina'],
        'professor' => $grade['professor']
    ];
}
unset($gradeHorariaDisciplina);
?>
<?=$PaginaHTML->getHead()?>
<?php $PaginaHTML->getHeader('inicio') ?>
<section id="main-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center"><?=$PaginaHTML->titulo?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p>Olá, <?=$Usuario->getNome()?>.</p>
                <p>Você pode ver sua grade horária do curso <strong><?=$curso?></strong> logo abaixo.</p>
            </div>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered text-center">
                        <thead>
                            <tr>
                                <th class="text-muted text-center">Horário</th>
                                <th class="text-center">Segunda</th>
                                <th class="text-center">Terça</th>
                                <th class="text-center">Quarta</th>
                                <th class="text-center">Quinta</th>
                                <th class="text-center">Sexta</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $intervalo = false;
                            foreach($gradeHoraria as $horario => $grade){
                                ?>
                                <tr>
                                    <td><?=$horario?></td>
                                    <?php
                                    foreach($grade as $aula) {
                                        ?>
                                        <td title="<?=$aula['disciplina']?>">
                                            <span><strong><?=$aula['sigla']?></strong></span>
                                            <span class="clearfix"><?=$aula['professor']?></span>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                </tr>
                                <?php
                                if(!$intervalo){
                                    $intervalo = true;
                                    ?>
                                    <tr>
                                        <td>20:40 às 20:50</td>
                                        <td><small>Intervalo</small></td>
                                        <td><small>Intervalo</small></td>
                                        <td><small>Intervalo</small></td>
                                        <td><small>Intervalo</small></td>
                                        <td><small>Intervalo</small></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?=$PaginaHTML->getFooter()?>