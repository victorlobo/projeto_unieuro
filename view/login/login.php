<?php
require(__DIR__ . '/../../autoload.php');
use controller\login\LoginController;
use view\PaginaHTML;

if(!empty($_POST)){
    $LoginController = new LoginController();
    $LoginController->login();
}
$PaginaHTML = new PaginaHTML();
$PaginaHTML->titulo = "Página de Login";
?>
<?=$PaginaHTML->getHead()?>
<?php $PaginaHTML->getHeader('inicio') ?>
<section id="main-content">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1 class="text-center"><?=$PaginaHTML->titulo?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="well">
                    <form action="/login/login.php" method="post">
                        <div class="form-group">
                            <label for="login">Login</label>
                            <input type="text" name="login" class="form-control" id="login" placeholder="Digite o login">
                        </div>
                        <div class="form-group">
                            <label for="senha">Senha</label>
                            <input type="password" name="senha" class="form-control" id="senha" placeholder="Digite a senha">
                        </div>
                        <div class="form-group">
                            <label for="tipoUsuario">Tipo de Usuário</label>
                            <select name="tipoUsuario" class="form-control" id="tipoUsuario">
                                <option value="" selected disabled hidden>Informe o tipo de usuario</option>
                                <option value="aluno">Aluno</option>
                                <option value="professor">Professor</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?=$PaginaHTML->getFooter()?>