<?php
namespace view;

class PaginaHTML
{
    public $titulo = "Unieuro";
    public $customStyles = array();
    public $customScripts = array();

    public function getHead(){
        $customStyles = '';
        foreach($this->customStyles as $style){
            $customStyles .= '<link rel="stylesheet" href="/view/assets/css/' . $style . '">';
        }
        $head = file_get_contents(__DIR__ . '/head.php');
        $head = str_replace('[CUSTOM_STYLES]', $customStyles, $head);
        $head = str_replace('[PAGE_TITLE]', $this->titulo, $head);
        return $head;
    }

    public function getHeader($active){
        require_once(__DIR__ . '/header.php');
        getHeader($active);
    }

    public function getFooter(){
        $customScripts = '';
        foreach($this->customScripts as $script){
            $customScripts .= ' <script src="/assets/bootstrap/js/' . $script . '"></script>';
        }
        $footer = file_get_contents(__DIR__ . '/footer.php');
        $footer = str_replace('[CUSTOM_SCRIPTS]', $customScripts, $footer);
        return $footer;
    }
}