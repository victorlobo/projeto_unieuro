<?php
namespace config;

class Connection
{
    const HOST = "localhost";
    const PORT = ":80";
    const DB = "unieuro";
    const USER = "root";
    const PASS = "";

    private $dbConnection = null;

    public function __construct()
    {
        $this->dbConnection = mysqli_connect(self::HOST, self::USER, self::PASS, self::DB);
        if (!$this->dbConnection) {
            echo "Error: Unable to connect to MySQL." . PHP_EOL;
            echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }
        $this->dbConnection->set_charset("utf8");
    }

    public function close(){
        mysqli_close($this->dbConnection);
        $this->dbConnection = null;
    }

    // Para Inserts, Updates e Deletes
    public static function executeSql($sql){
        $Connection = new Connection();
        if ($Connection->dbConnection->connect_errno){
            echo "Falha ao conectar ao MySQL: (" . $Connection->dbConnection->connect_errno . ") " . $Connection->dbConnection->connect_error;
            $Connection->close();
            return false;
        }
        /* Prepared statement, stage 1: prepare */
        $stmt = $Connection->dbConnection->prepare($sql);
        if (!$stmt) {
            echo "Prepare failed: (" . $Connection->dbConnection->errno . ") " . $Connection->dbConnection->error;
            $Connection->close();
            return false;
        }
        $rs = $stmt->execute();
        $Connection->close();
        return $rs;
    }

    // Para Selects
    public static function fetchSql($sql, $qtd = "all"){
        $Connection = new Connection();
        if ($Connection->dbConnection->connect_errno){
            echo "Falha ao conectar ao MySQL: (" . $Connection->dbConnection->connect_errno . ") " . $Connection->dbConnection->connect_error;
            $Connection->close();
            return false;
        }
        /* Prepared statement, stage 1: prepare */
        $stmt = $Connection->dbConnection->query($sql);
        if (!$stmt) {
            echo "Prepare failed: (" . $Connection->dbConnection->errno . ") " . $Connection->dbConnection->error;
            $Connection->close();
            return false;
        }
        $rs = false;
        if($qtd == "all"){
            $rs = (object) $stmt->fetch_all(MYSQLI_ASSOC);
        }
        else if($qtd == "row"){
            $rs = (object) $stmt->fetch_assoc();
        }
        $Connection->close();
        return $rs;
    }
}